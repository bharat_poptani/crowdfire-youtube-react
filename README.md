Please follow the following steps to kickstart the app:
1. Run 'npm install' in your terminal.
2. After all the packages get installed using the previous step, run 'npm start'
3. Go to your browser and type in 'http://localhost:8080/' in the address bar
