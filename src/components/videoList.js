import React, { Component } from 'react';
import VideoListItem from './videoListItem';

class VideoList extends Component {

	render() {
		const VideoItems = this.props.videos.map( video => {
			return <VideoListItem
				onVideoSelect={this.props.onVideoSelect}
				key={video.etag}
				video={video} />;
		});

		return (
			<ul className="col-md-4 list-group list-videos">
			{VideoItems}
			</ul>
			);
	}
}

export default VideoList;
