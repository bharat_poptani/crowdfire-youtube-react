import React, { Component } from 'react';

class SearchBar extends Component {
	constructor(props) {
		super(props);

		this.state = { 'searchText': '' };
	}

	render() {
		return (
			<div className="search-bar">
				<input
					value={this.state.searchText}
					onChange={event => this.onInputChange(event.target.value)}/>
			</div>
		);
	}

	onInputChange(searchQuery) {
		this.setState({'searchText': searchQuery});
		this.props.onSearchQueryChange(searchQuery);
	}
}

export default SearchBar;
