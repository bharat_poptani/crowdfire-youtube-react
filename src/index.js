import React, { Component } from 'react';
import ReactDOM from 'react-dom';
// import YTsearch from 'youtube-api-search';
import axios from 'axios';
import _ from 'lodash';

import SearchBar from './components/searchBar';
import VideoList from './components/videoList';
import SpotlightVideo from './components/spotlightVideo';

const YOUTUBE_API_KEY = 'AIzaSyD7il7__dBPHupujfh1Byn9_vpxqEnBXgg';
const ROOT_URL = 'https://www.googleapis.com/youtube/v3/search';

function YTsearch(options, callback) {
  if (!options.key) {
    throw new Error('Youtube Search expected key, received undefined');
  }

  var params = {
    part: 'snippet',
    key: options.key,
    q: options.term,
    type: 'video',
    maxResults: 20,
    'pageToken': options.pageToken ? options.pageToken : ''
  };

  axios.get(ROOT_URL, { params: params })
    .then(function(response) {
      if (callback) { callback(response.data); }
    })
    .catch(function(error) {
      console.error(error);
    });
}

class YoutubeApp extends Component {
	constructor(props) {
		super(props);

		this.state = {
			'videos': [],
			'selectedVideo': null,
			'nextPage': '',
			'term': ''
		};

		//Initial search on load
		this.videoSearch('Conor McGregor');
	}

	videoSearch(searchQuery, pageTok) {
		YTsearch({
			'key': YOUTUBE_API_KEY,
			'term': searchQuery,
			'pageToken': pageTok ? pageTok : ''
		}, videos => {
			let currentVideos;

			if (this.state.videos.length === 0 || this.state.term !== searchQuery) {
				this.setState({
					'videos': videos.items,
					'selectedVideo': videos.items[0],
					'nextPage': videos.nextPageToken,
					'term': searchQuery
				});
			} else {

				this.setState({
					'videos': this.state.videos.concat(videos.items),
					'nextPage': videos.nextPageToken
				});
			}
		});
	}

	render() {
		const videoSearch = _.debounce((searchQuery) => { this.videoSearch(searchQuery); }, 300);

		return (
			<div>
				<SearchBar onSearchQueryChange={videoSearch} />
				<SpotlightVideo video={this.state.selectedVideo} />
				<VideoList
					onVideoSelect={selectedVideo => this.setState({selectedVideo})}
					videos={this.state.videos}
					videoSearch={this.videoSearch.bind(this)}
					searchTerm={this.state.term}
					nextPage={this.state.nextPage}
				/>

				<button onClick={() => {
					this.videoSearch(this.state.term, this.state.nextPage);
				}}>Load More</button>
			</div>
		);
	}
}

ReactDOM.render(<YoutubeApp />, document.querySelector('.container'));
